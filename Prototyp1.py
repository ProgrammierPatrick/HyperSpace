#!/usr/bin/env python3

import numpy as np
import pygame as pg
from OpenGL.GL import *
from OpenGL.GLU import *

width = 1600
height = 900

# Controls:
############
#
# Movement:
#   [A] / [D] : move X direction
#   [SHIFT] / [SPACE]: move Y direction
#   [S] / [W] : move Z direction
#   [Q] / [E] : move W direction
#
# Rotation: (around camera origin)
#   [LEFT] / [RIGHT]   : XZ
#   [UP] / [DOWN]      : YZ
#
#   only use one of:
#   [PGUP] / [PGDN]    : XW
#   [COMMA] / [PERIOD] : ZW




# vector: v = np.array([1,2,3])
# dot product: np.dot(v1, v2)

# matrix: M = np.matrix( ((1,2), (3,4)) )

# AABB with min/max corners

objects = [[np.array([0.,0.,0.,0.]),np.array([1.,1.,1.,1.])],
			[np.array([ 2, 0, 0, 0]), np.array([2.1,1,.1,.1])],
			[np.array([-5,-.1,-5,-5]), np.array([5,.1,5,5])]   ]

class Camera:
	pos = np.array([0,.5,-1.,-1.])
	rotYZ = 0.
	rotXZ = 0.
	rotZW = 0.
	rotXW = 0.
	
cam = Camera()

def transform(vector):
#	print("Transform", vector)

	v = vector - cam.pos
#	print("Translated:", v)
	return project(rotate(v))

def rotate(vector):
	v = np.transpose(vector[np.newaxis])

	rotXW = np.matrix( ((np.cos(cam.rotXW), 0, 0, np.sin(cam.rotXW)),
						(0,                 1, 0, 0),
						(0,                 0, 1, 0),
						(-np.sin(cam.rotXW),0, 0, np.cos(cam.rotXW))))
	v = rotXW * v
	rotZW = np.matrix( ((1, 0, 0, 0),
						(0, 1, 0, 0),
						(0, 0, np.cos(cam.rotZW), np.sin(cam.rotZW)),
						(0, 0,-np.sin(cam.rotZW), np.cos(cam.rotZW))))
	v = rotZW * v

	rotXZ = np.matrix( ((np.cos(cam.rotXZ), 0, np.sin(cam.rotXZ), 0),
						(0,                 1, 0,                 0),
						(-np.sin(cam.rotXZ),0, np.cos(cam.rotXZ), 0),
						(0,                 0, 0,                 1)))
	v = rotXZ * v

	rotYZ = np.matrix( ((1, 0,                 0,                 0),
						(0, np.cos(cam.rotYZ), np.sin(cam.rotYZ), 0),
						(0,-np.sin(cam.rotYZ), np.cos(cam.rotYZ), 0),
						(0, 0,                 0,                 1)))
	v = rotYZ * v

	# (x,y,z,w) |--> (x/w, y/w, z/w) |--> (x/z, y/z)
	# (x,y,z,w) |--> (x,   y,   z)   |--> (x/z, y/z)
	v = v.A1
	return v

def project(v):
	v = np.array([ v[0], v[1], -v[2] ]) #let opengl do last step
#	v = np.array([v[0]/v[3], v[1]/v[3], -v[2]/v[3]])
#	print("Projected:", v)

	return v
	
def transformCube(corners):
	# just normal 3-Cube for testing
	a = transform(np.array([corners[0][0], corners[0][1], corners[0][2], corners[0][3]]))
	b = transform(np.array([corners[1][0], corners[0][1], corners[0][2], corners[0][3]]))
	c = transform(np.array([corners[0][0], corners[1][1], corners[0][2], corners[0][3]]))
	d = transform(np.array([corners[1][0], corners[1][1], corners[0][2], corners[0][3]]))
	e = transform(np.array([corners[0][0], corners[0][1], corners[1][2], corners[0][3]]))
	f = transform(np.array([corners[1][0], corners[0][1], corners[1][2], corners[0][3]]))
	g = transform(np.array([corners[0][0], corners[1][1], corners[1][2], corners[0][3]]))
	h = transform(np.array([corners[1][0], corners[1][1], corners[1][2], corners[0][3]]))
	i = transform(np.array([corners[0][0], corners[0][1], corners[0][2], corners[1][3]]))
	j = transform(np.array([corners[1][0], corners[0][1], corners[0][2], corners[1][3]]))
	k = transform(np.array([corners[0][0], corners[1][1], corners[0][2], corners[1][3]]))
	l = transform(np.array([corners[1][0], corners[1][1], corners[0][2], corners[1][3]]))
	m = transform(np.array([corners[0][0], corners[0][1], corners[1][2], corners[1][3]]))
	n = transform(np.array([corners[1][0], corners[0][1], corners[1][2], corners[1][3]]))
	o = transform(np.array([corners[0][0], corners[1][1], corners[1][2], corners[1][3]]))
	p = transform(np.array([corners[1][0], corners[1][1], corners[1][2], corners[1][3]]))
	list = [(a,b,d,c),(a,b,f,e),(b,d,h,f),(d,c,g,h),(a,c,g,e),(e,f,h,g),
			(i,j,l,k),(i,j,n,m),(j,l,p,n),(k,l,p,o),(i,k,o,m),(m,n,p,o),
			(a,e,m,i),(b,f,n,j),(d,h,p,l),(c,g,o,k),
			(a,b,j,i),(b,d,l,j),(c,d,l,k),(a,c,k,i),
			(e,f,n,m),(f,h,p,n),(g,h,p,o),(e,g,o,m)]
	return list

def renderQuads(list):
	for (a,b,c,d) in list:
#		print(a,b,c,d)
		glBegin(GL_QUADS)
		glColor3f(.2, .2, 0)
		glVertex3f(a[0], a[1], a[2])
		glVertex3f(b[0], b[1], b[2])
		glVertex3f(c[0], c[1], c[2])
		glVertex3f(d[0], d[1], d[2])
		glEnd()

def renderQuadLines(list):
	for (a,b,c,d) in list:
#		print(a,b,c,d)
		glBegin(GL_LINE_STRIP)
		glColor3f(1.,1.,1.)
		glVertex3f(a[0], a[1], a[2])
		glVertex3f(b[0], b[1], b[2])
		glVertex3f(c[0], c[1], c[2])
		glVertex3f(d[0], d[1], d[2])
		glVertex3f(a[0], a[1], a[2])
		glEnd()

def renderLine( a,b,col ):
	glBegin(GL_LINES)
	glColor3f(col[0], col[1], col[2])
	glVertex3f(a[0], a[1], a[2])
	glVertex3f(b[0], b[1], b[2])
	glEnd()

def renderAxes():
	l = .2
	p = np.array([0,0,1,0])
	x = p + rotate(np.array([l,0,0,0]))
	y = p + rotate(np.array([0,l,0,0]))
	z = p + rotate(np.array([0,0,l,0]))
	w = p + rotate(np.array([0,0,0,l]))
	renderLine(project(p),project(x),(1,0,0))
	renderLine(project(p),project(y),(0,1,0))
	renderLine(project(p),project(z),(0,0,1))
	renderLine(project(p),project(w),(1,1,0))

pg.init()
scr = pg.display.set_mode((width,height), pg.OPENGL | pg.DOUBLEBUF)
pg.display.set_caption("HyperSpace Prototype 1: 4D-2D full perspective")

glEnable(GL_DEPTH_TEST)
glViewport(0,0,width,height)
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(70, 16.0/9.0, 0.1, 1000.0)
#glOrtho(-2,2,-2,2, -1, 1)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
glClearColor(0.0, 0.0, 0.0, 1.0)

clk = pg.time.Clock()
running = True
while running:
	clk.tick(60)
	for event in pg.event.get():
		if event.type == pg.QUIT: running = False
		if event.type == pg.KEYDOWN:
			if event.key == pg.K_ESCAPE:
				pg.event.post(pg.event.Event(pg.QUIT))

	bt = pg.key.get_pressed()
	ms = 0.05 # move speed
	rs = 0.02 # rotate speed
	if bt[pg.K_RIGHT]: cam.rotXZ -= rs
	if bt[pg.K_LEFT]: cam.rotXZ += rs
	if bt[pg.K_COMMA]: cam.rotZW -= rs
	if bt[pg.K_PERIOD]: cam.rotZW += rs
	if bt[pg.K_UP]: cam.rotYZ -= rs
	if bt[pg.K_DOWN]: cam.rotYZ += rs
	if bt[pg.K_PAGEUP]: cam.rotXW += rs
	if bt[pg.K_PAGEDOWN]: cam.rotXW -= rs
	if bt[pg.K_w]: cam.pos += np.array([0,0,ms,0])
	if bt[pg.K_s]: cam.pos += np.array([0,0,-ms,0])
	if bt[pg.K_a]: cam.pos += np.array([-ms,0,0,0])
	if bt[pg.K_d]: cam.pos += np.array([ms,0,0,0])
	if bt[pg.K_q]: cam.pos += np.array([0,0,0,-ms])
	if bt[pg.K_e]: cam.pos += np.array([0,0,0,ms])
	if bt[pg.K_SPACE]: cam.pos += np.array([0,ms,0,0])
	if bt[pg.K_LSHIFT]: cam.pos -= np.array([0,ms,0,0])
	
	
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

	for cube in objects:
		quads = transformCube(cube)
		renderQuads(quads)

	glDisable(GL_DEPTH_TEST)

	for cube in objects:
		quads = transformCube(cube)
		renderQuadLines(quads)

	renderAxes()

	glEnable(GL_DEPTH_TEST)

	pg.display.flip()
