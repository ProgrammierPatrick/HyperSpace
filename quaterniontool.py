#!/usr/bin/env python3
class Symbol:
    def __init__(self, s):
        self.v = s
    def __mul__(self, v2):
        a = "(" + self.v + ")" if "+" in self.v or "-" in self.v else self.v
        b = "(" + v2.v + ")" if "+" in v2.v or "-" in v2.v else v2.v

        return Symbol(a + "*" + b)

    def __add__(self, v2):
        if self.v == "": return v2
        if v2.v == "": return self
        return Symbol(self.v + "+" + v2.v)
    def __sub__(self, v2):
        if self.v == "": return v2
        if v2.v == "": return self
        return Symbol(self.v + "-" + v2.v)
    def __str__(self):
        return self.v

class Quaternion:
    def __init__(self, a,b,c,d):
        self.v = [a,b,c,d]

    def __str__(self):
        return str(self.v[0]) + "+i(" + str(self.v[1]) + ")+j(" + str(self.v[2]) + ")+k(" + str(self.v[3]) + ")"

    def __add__(self, q2):
        return Quaternion(
            self.v[0] + q2.v[0],
            self.v[1] + q2.v[1],
            self.v[2] + q2.v[2],
            self.v[3] + q2.v[3])

    def __mul__(self, q2):
        q1 = self.v
        q2 = q2.v
        q = Quaternion(Symbol(""), Symbol(""), Symbol(""), Symbol(""))
        for i in range(4):
            for j in range(4):
                val = q1[i] * q2[j]
                if i == 0:
                    q.v[j] += val
                elif j == 0:
                    q.v[i] += val
                elif i == j:
                    q.v[0] -= val
                elif i == 1 and j == 2:
                    q.v[3] += val
                elif i == 2 and j == 3:
                    q.v[1] += val
                elif i == 3 and j == 1:
                    q.v[2] += val
                elif i == 2 and j == 1:
                    q.v[3] -= val
                elif i == 3 and j == 2:
                    q.v[1] -= val
                elif i == 1 and j == 3:
                    q.v[2] -= val
                else: print("error")

        return q
    def conjungate(self):
        n = Symbol("")
        return Quaternion(self.v[0], n-self.v[1], n-self.v[2], n-self.v[3])

q1 = Quaternion(Symbol("a"),Symbol("b"),Symbol("c"),Symbol("d"))
q2 = Quaternion(Symbol("w"),Symbol("x"),Symbol("y"),Symbol("z"))

# print(q1 + q2)
print(q1 * q2 * q1.conjungate())
