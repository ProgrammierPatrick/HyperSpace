#!/usr/bin/env python3

import numpy as np
import pygame as pg
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

width = 1600
height = 900

Ax = np.array([1.,0.,0.,0.])
Ay = np.array([0.,1.,0.,0.])
Az = np.array([0.,0.,1.,0.])
Aw = np.array([0.,0.,0.,1.])
R = np.array([[np.cos(0.01), np.sin(0.01)],[-np.sin(0.01),np.cos(0.01)]])
I = np.diag([1.,1.,1.,1.])
# Controls:
############
#
# Movement:
#   [A] / [D] : move X direction
#   [SHIFT] / [SPACE]: move Y direction
#   [S] / [W] : move Z direction
#   [Q] / [E] : move W direction
#
# Rotation: (around camera origin)
#   [LEFT] / [RIGHT]   : XZ
#   [UP] / [DOWN]      : YZ
#
#   only use one of:
#   [PGUP] / [PGDN]    : XW
#   [COMMA] / [PERIOD] : ZW




# vector: v = np.array([1,2,3])
# dot product: np.dot(v1, v2)

# matrix: M = np.matrix( ((1,2), (3,4)) )

# AABB with min/max corners

objects = [[np.array([0.,0.,0.,0.]),np.array([1.,1.,1.,1.])],
            [np.array([ 2, 0, 0, 0]), np.array([2.1,1,.1,.1])],
            [np.array([-5,-.1,-5,-5]), np.array([5,.1,5,5])]   ]

class Cam:
    _rot = np.diag([1.,1.,1.,1.])
    _pos = np.zeros(4)
    def rot(self,a,b,r):
        """
            Rotates the scene on the a,b Plane with rotation r
            a,b are two orthonormal vectors
            r is a 2D-Rotation Matrix
        """
        #Ortonormalize
        na = 1./np.linalg.norm(a)
        a*=na
        ab = np.dot(a,b)
        b-=ab*a
        nb = 1./np.linalg.norm(b)
        b*=nb

        #Calculate Rotation
        M_T = np.array([a,b])
        M = np.transpose(M_T)
        X=np.dot(M,np.dot(r,M_T))+I-np.dot(M,M_T)

        #Apply Rotation
        self._rot=np.dot(X,self._rot)

        #Correct Rotation
        U,s,V = np.linalg.svd(self._rot)
        self._rot = np.dot(U,V)


    def move(self, v):
        self._pos += np.dot(np.transpose(self._rot),v)

    def get_rot(self):
        return self._rot

    def get_pos(self):
        return self._pos

    def __str__(self):
        return str(self._pos)+"\n"+str(self._rot)

cam = Cam()

def transform(vector):
#   print("Transform", vector)

    v = vector - cam.get_pos()
#   print("Translated:", v)
    return project(rotate(v))

def rotate(vector):
    return np.dot(cam.get_rot(), vector)

def project(v):
    v = np.array([ v[0], v[1], -v[2] ]) #let opengl do last step
#   v = np.array([v[0]/v[3], v[1]/v[3], -v[2]/v[3]])
#   print("Projected:", v)

    return v
    
def transformCube(corners):
    # just normal 3-Cube for testing
    a = transform(np.array([corners[0][0], corners[0][1], corners[0][2], corners[0][3]]))
    b = transform(np.array([corners[1][0], corners[0][1], corners[0][2], corners[0][3]]))
    c = transform(np.array([corners[0][0], corners[1][1], corners[0][2], corners[0][3]]))
    d = transform(np.array([corners[1][0], corners[1][1], corners[0][2], corners[0][3]]))
    e = transform(np.array([corners[0][0], corners[0][1], corners[1][2], corners[0][3]]))
    f = transform(np.array([corners[1][0], corners[0][1], corners[1][2], corners[0][3]]))
    g = transform(np.array([corners[0][0], corners[1][1], corners[1][2], corners[0][3]]))
    h = transform(np.array([corners[1][0], corners[1][1], corners[1][2], corners[0][3]]))
    i = transform(np.array([corners[0][0], corners[0][1], corners[0][2], corners[1][3]]))
    j = transform(np.array([corners[1][0], corners[0][1], corners[0][2], corners[1][3]]))
    k = transform(np.array([corners[0][0], corners[1][1], corners[0][2], corners[1][3]]))
    l = transform(np.array([corners[1][0], corners[1][1], corners[0][2], corners[1][3]]))
    m = transform(np.array([corners[0][0], corners[0][1], corners[1][2], corners[1][3]]))
    n = transform(np.array([corners[1][0], corners[0][1], corners[1][2], corners[1][3]]))
    o = transform(np.array([corners[0][0], corners[1][1], corners[1][2], corners[1][3]]))
    p = transform(np.array([corners[1][0], corners[1][1], corners[1][2], corners[1][3]]))
    list = [(a,b,d,c),(a,b,f,e),(b,d,h,f),(d,c,g,h),(a,c,g,e),(e,f,h,g),
            (i,j,l,k),(i,j,n,m),(j,l,p,n),(k,l,p,o),(i,k,o,m),(m,n,p,o),
            (a,e,m,i),(b,f,n,j),(d,h,p,l),(c,g,o,k),
            (a,b,j,i),(b,d,l,j),(c,d,l,k),(a,c,k,i),
            (e,f,n,m),(f,h,p,n),(g,h,p,o),(e,g,o,m)]
    return list

def renderQuads(list):
    for (a,b,c,d) in list:
#       print(a,b,c,d)
        glBegin(GL_QUADS)
        glColor3f(.2, .2, 0)
        glVertex3f(a[0], a[1], a[2])
        glVertex3f(b[0], b[1], b[2])
        glVertex3f(c[0], c[1], c[2])
        glVertex3f(d[0], d[1], d[2])
        glEnd()

def renderQuadLines(list):
    for (a,b,c,d) in list:
#       print(a,b,c,d)
        glBegin(GL_LINE_STRIP)
        glColor3f(1.,1.,1.)
        glVertex3f(a[0], a[1], a[2])
        glVertex3f(b[0], b[1], b[2])
        glVertex3f(c[0], c[1], c[2])
        glVertex3f(d[0], d[1], d[2])
        glVertex3f(a[0], a[1], a[2])
        glEnd()

def renderLine( a,b,col ):
    glBegin(GL_LINES)
    glColor3f(col[0], col[1], col[2])
    glVertex3f(a[0], a[1], a[2])
    glVertex3f(b[0], b[1], b[2])
    glEnd()

def renderAxes():
    l = .2
    p = np.array([0,0,1,0])
    x = p + rotate(np.array([l,0,0,0]))
    y = p + rotate(np.array([0,l,0,0]))
    z = p + rotate(np.array([0,0,l,0]))
    w = p + rotate(np.array([0,0,0,l]))
    renderLine(project(p),project(x),(1,0,0))
    renderLine(project(p),project(y),(0,1,0))
    renderLine(project(p),project(z),(0,0,1))
    renderLine(project(p),project(w),(1,1,0))

def drawText( value, x,y,  windowHeight, windowWidth, step = 18 ):
    """Draw the given text at given 2D position in window
    """
    glMatrixMode(GL_PROJECTION);
    # For some reason the GL_PROJECTION_MATRIX is overflowing with a single push!
    # glPushMatrix()
    matrix = glGetDouble( GL_PROJECTION_MATRIX )
    
    glLoadIdentity();
    glOrtho(0.0, windowHeight or 32, 0.0, windowWidth or 32, -1.0, 1.0)
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glRasterPos2i(x, y);
    lines = 0
##	import pdb
##	pdb.set_trace()
    for character in value:
        if character == '\n':
            glRasterPos2i(x, y-(lines*18))
        else:
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, ord(character));
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    # For some reason the GL_PROJECTION_MATRIX is overflowing with a single push!
    # glPopMatrix();
    glLoadMatrixd( matrix ) # should have un-decorated alias for this...
    
    glMatrixMode(GL_MODELVIEW);

pg.init()
scr = pg.display.set_mode((width,height), pg.OPENGL | pg.DOUBLEBUF)
pg.display.set_caption("HyperSpace Prototype 1: 4D-2D full perspective")

glEnable(GL_DEPTH_TEST)
glViewport(0,0,width,height)
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(70, 16.0/9.0, 0.1, 1000.0)
#glOrtho(-2,2,-2,2, -1, 1)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
glClearColor(0.0, 0.0, 0.0, 1.0)
glutInit()

clk = pg.time.Clock()
running = True
while running:
    clk.tick(60)
    for event in pg.event.get():
        if event.type == pg.QUIT: running = False
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_ESCAPE:
                pg.event.post(pg.event.Event(pg.QUIT))

    bt = pg.key.get_pressed()
    ms = 0.05 # move speed
    rs = 0.02 # rotate speed
    if bt[pg.K_RIGHT]:
        cam.rot(Az,Ax,R)
    if bt[pg.K_LEFT]:
        cam.rot(Ax,Az,R)
    if bt[pg.K_COMMA]:
        cam.rot(Ay,Ax,R)
    if bt[pg.K_PERIOD]:
        cam.rot(Ax,Ay,R)
    if bt[pg.K_UP]:
        cam.rot(Az,Ay,R)
    if bt[pg.K_DOWN]:
        cam.rot(Ay,Az,R)
    if bt[pg.K_PAGEUP]:
        cam.rot(Ax,Aw,R)
    if bt[pg.K_PAGEDOWN]:
        cam.rot(Aw,Ax,R)
    if bt[pg.K_w]: cam.move(np.array([0,0,ms,0]))
    if bt[pg.K_s]: cam.move(np.array([0,0,-ms,0]))
    if bt[pg.K_a]: cam.move(np.array([-ms,0,0,0]))
    if bt[pg.K_d]: cam.move(np.array([ms,0,0,0]))
    if bt[pg.K_q]: cam.move(np.array([0,0,0,-ms]))
    if bt[pg.K_e]: cam.move(np.array([0,0,0,ms]))
    if bt[pg.K_SPACE]: cam.move(np.array([0,-ms,0,0]))
    if bt[pg.K_LSHIFT]: cam.move(np.array([0,ms,0,0]))
    
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

    for cube in objects:
        quads = transformCube(cube)
        renderQuads(quads)

    glDisable(GL_DEPTH_TEST)

    for cube in objects:
        quads = transformCube(cube)
        renderQuadLines(quads)

    renderAxes()
    glDisable( GL_LIGHTING )
    drawText( "%5.2f,%5.2f,%5.2f,%5.2f"% (cam.get_pos()[0],cam.get_pos()[1],cam.get_pos()[2],cam.get_pos()[3]), 0,99,  100, 102, step = 18 )

    glEnable(GL_DEPTH_TEST)

    pg.display.flip()
