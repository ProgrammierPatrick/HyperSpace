#!/usr/bin/env python3

import numpy as np
import pygame as pg
from OpenGL.GL import *
from OpenGL.GLU import *

width = 1600
height = 900

"""
https://s3pm.icsi.berkeley.edu/s3pm/papers/SMI-Poster-P-06.pdf
https://dl.acm.org/citation.cfm?id=3140001
Algorithm:

    base change:
    new base: direction vectors of parallelepid at 0

    ax+by+cz+dw=1 (plane to cut with)

    pyramid with cube border x>0,y>0,z>0,w>0
    (1/a,0,0,0),(0,1/b,0,0),(0,0,1/c,0),(0,0,0,1/d)

    pyramid with cube border x<1,y<1,z<1,w<1
    ((1-b-c-d)/a,0,0,0),(0,(1-a-c-d)/b,0,0),(0,0,(1-a-b-d)/c,0),(0,0,0,(1-a-b-c)/d)

    for every polygon in pyramid 1:
        clip with all 4 planes of pyramid 2 with sutherland algorithm
        render clipped polygon
"""

# Controls:
############
#
# Movement:
#   [A] / [D] : move X direction
#   [SHIFT] / [SPACE]: move Y direction
#   [S] / [W] : move Z direction
#   [Q] / [E] : move W direction
#
# Rotation: (around camera origin)
#   [LEFT] / [RIGHT]   : XZ
#   [UP] / [DOWN]      : YZ
#   [PGUP] / [PGDN]    : XW & ZW




# vector: v = np.array([1,2,3])
# dot product: np.dot(v1, v2)

# matrix: M = np.matrix( ((1,2), (3,4)) )

# AABB with min/max corners

"""
# complex test scene
objects =  [
			([np.array([ 0.0, 0.0,-0.5, 0.0]),np.array([ 3.0, 3.0, 0.0, 0.1])], (0.5,0.5,0.5)),
			([np.array([-0.5, 0.0, 0.0, 0.0]),np.array([ 0.0, 3.0, 4.0, 0.1])], (0.6,0.6,0.6)),
			([np.array([ 2.5, 0.0, 0.0, 0.0]),np.array([ 3.0, 3.0, 4.0, 0.1])], (0.6,0.6,0.6)),
			([np.array([ 0.0, 0.5, 4.0, 0.0]),np.array([ 3.0, 1.0, 4.5, 0.1])], (0.7,0.7,0.7)),
			([np.array([ 0.0, 2.0, 4.0, 0.0]),np.array([ 3.0, 3.0, 4.5, 0.1])], (0.7,0.7,0.7)),
			([np.array([-1.0, 0.0, 0.0,-1.5]),np.array([-1.0, 3.0, 4.0,-1.0])], (0.8,0.8,0.8)),
			([np.array([ 0.0, 1.0, 4.0, 0.0]),np.array([ 1.0, 2.0, 4.5, 0.1])], (0.7,0.7,0.7)),
			([np.array([ 2.0, 1.0, 4.0, 0.0]),np.array([ 3.0, 2.0, 4.5, 0.1])], (0.7,0.7,0.7)),
			([np.array([ 0.0,-0.5, 0.0,-1.0]),np.array([ 3.0, 0.0, 2.0, 0.1])], (102./256,57./256,0.0)),
			([np.array([ 0.0, 0.0, 2.0,-1.0]),np.array([ 3.0, 0.5, 4.0, 0.1])], (112./256,57./256,0.0)),
			([np.array([ 0.0,-1.0, 0.0,-1.0]),np.array([ 3.0,-0.5, 6.0, 5.0])], (0.1,0.9,0.1)),
			([np.array([ 0.0, 0.0, 0.5, 0.0]),np.array([ 0.3, 0.5, 1.5, 0.1])], (0.2,0.2,0.2)),
			([np.array([ 0.0, 0.5, 0.5, 0.0]),np.array([ 0.1, 1.0, 1.5, 0.1])], (0.2,0.2,0.2)),
			([np.array([ 1.0,-0.5, 0.5, 3.0]),np.array([ 1.5, 0.5, 1.5, 3.5])], (9.0,0.1,0.1))]
"""

# simple test scene (single cube)
objects = [([np.array([-1,-1,-1,-1]), np.array([1,1,1,1])], (1,.5,.5))]

# swap x and z because of wrong design
obj = []
for ([a,b], col) in objects:
	obj.append(([np.array([a[2],a[1],a[0],a[3]]),np.array([b[2],b[1],b[0],b[3]])], col))
objects = obj

class Camera:
	pos = np.array([-5., 0., 0., 0.])
	rotYZ = 0.
	rotXZ = -np.pi /2
	rotZW = 0.
	rotXW = 0.
	rotYW = 0.001
	
cam = Camera()

def transform(vector):
#	print("Transform", vector)

	v = vector - cam.pos
#	print("Translated:", v)
	return rotate(v)

# a: factor for rotation, ex. -1 for inverse rotation
def rotate(vector, reverse=False):
	a = 1.
	if reverse: a = -1
	v = np.transpose(vector[np.newaxis])

	rotXW = np.matrix( ((np.cos(cam.rotXW*a), 0, 0, np.sin(cam.rotXW*a)),
						(0,                   1, 0, 0),
						(0,                   0, 1, 0),
						(-np.sin(cam.rotXW*a),0, 0, np.cos(cam.rotXW*a))))
	rotYW = np.matrix( ((1, 0,                   0, 0),
						(0, np.cos(cam.rotYW*a), 0, np.sin(cam.rotYW*a)),
						(0, 0,                   1, 0),
						(0,-np.sin(cam.rotYW*a), 0, np.cos(cam.rotYW*a))))
	rotZW = np.matrix( ((1, 0, 0, 0),
						(0, 1, 0, 0),
						(0, 0, np.cos(cam.rotZW*a), np.sin(cam.rotZW*a)),
						(0, 0,-np.sin(cam.rotZW*a), np.cos(cam.rotZW*a))))

	rotXZ = np.matrix( ((np.cos(cam.rotXZ*a), 0, np.sin(cam.rotXZ*a), 0),
						(0,                   1, 0,                   0),
						(-np.sin(cam.rotXZ*a),0, np.cos(cam.rotXZ*a), 0),
						(0,                   0, 0,                   1)))

	rotYZ = np.matrix( ((1, 0,                   0,                   0),
						(0, np.cos(cam.rotYZ*a), np.sin(cam.rotYZ*a), 0),
						(0,-np.sin(cam.rotYZ*a), np.cos(cam.rotYZ*a), 0),
						(0, 0,                   0,                   1)))

	if reverse: v = rotXW * rotYW * rotZW * rotXZ * rotYZ * v
	else: v = rotYZ * rotXZ * rotZW * rotYW * rotXW * v

	# (x,y,z,w) |--> (x/w, y/w, z/w) |--> (x/z, y/z)
	# (x,y,z,w) |--> (x,   y,   z)   |--> (x/z, y/z)
	v = v.A1
	return v

#################### ALGORITHM START ################################
def computePoints(p0, p1, p2, p3, p4):
    # base change matrix from parallelogram coordinates to world coordinates
    PStoWS = np.array([
        [p1[0], p2[0], p3[0], p4[0], p0[0]],
        [p1[1], p2[1], p3[1], p4[1], p0[1]],
        [p1[2], p2[2], p3[2], p4[2], p0[2]],
        [p1[3], p2[3], p3[3], p4[3], p0[3]],
        [0,     0,     0,     0,     1]])
#    print(PStoWS)
    points = [] 
    for i in range(16):
        # point numbering: in binary: wzyx  (1011 => (1,1,0,1))
        p = np.array([i&1, (i&2)>>1, (i&4)>>2, (i&8)>>3, 1]) #1: homogenus coordinates
        #print(p)
        points.append(PStoWS.dot(p))

    return points

def bitCount(int_type):
    count = 0
    while(int_type):
        int_type &= int_type - 1
        count += 1
    return(count)

def computeEdgeIntersections(p0, p1, p2, p3, p4, points):
    ws = np.array([p1[3], p2[3], p3[3], p4[3], 0])
    intersections = []
    for i in range(16):
        c1 = points[i]
        for j in range(i+1,16):
            if bitCount(i ^ j) > 1: continue # these points share no edge, edge means only one bit changes

            c2 = points[j]
            if np.sign(p1[3]) * np.sign(p2[3]) > 0: # continue if same site and none is =0
                continue

            alpha = -ws.dot(points[i]) / ws.dot(points[j] - points[i])
            if alpha < 0 or alpha > 1: continue
            c = c1 + alpha * (c2 - c1)
            intersections.append( (i,j,c) )
            #print(i,j,alpha)
    return intersections

def computeFaces(points, intersections):
    faces = []
    for (dynmask,fixmask) in [(0b1110,1),(0b1101,2),(0b1011,4),(0b0111,8)]: # bitmasks for variable directions and fixed direction
        for side in [0,1]:
            #print("Volume",bin(dynmask), side)
            points = []
            for (i,j,c) in intersections:
                if np.sign(i & fixmask) != side: continue #only interested in points at this volume
                if np.sign(j & fixmask) != side: continue #only interested in points at this volume
                points.append( (i,j,c) )
                #print("Edge",i,j)

            first = True
            (i, j) = (0, 0)
            (lasti, lastj) = (-1, -1) #-1: should not discard anything at start
            face = []
            while len(points) > 0:
                if first:
                    (i, j, startPoint) = points.pop()
                    #print("i,j,startPoint", i,j,startPoint)
                    face.append(startPoint)
                    first = False
                    #print("Start ij:",i,j)
                
                found = False

                axis = i ^ j #bit that changes


                # connected edges
                for newaxis in [1,2,4,8]:
                    if newaxis == axis or newaxis == fixmask: continue
                    for oldaxisflip in [0,1]:
                        newi = ~(newaxis | axis) & i | oldaxisflip & axis
                        newj = ~(newaxis | axis) & j | oldaxisflip & axis | newaxis
                    
                        for (ii,jj,pp) in points:
                            if ii == newi and jj == newj:
                                # found next intersection
                                lasti = i
                                lastj = j
                                i = newi
                                j = newj
                                points.remove( (ii,jj,pp) )
                                face.append(pp)
                                #print("connected:", i,j)
                                found = True
                                break
                            if found == True: break

                    if found == True: continue

                #parallel edges:
                for bitflip in [1,2,4,8]:
                    if bitflip == axis or bitflip == fixmask: continue
                    newi = i ^ bitflip
                    newj = j ^ bitflip
                    for (ii,jj,pp) in points:
                        if ii == newi and jj == newj:
                            # found next intersection
                            lasti = i
                            lastj = j
                            i = newi
                            j = newj
                            points.remove( (ii,jj,pp) )
                            face.append(pp)
                            #print("parallel:", i,j)
                            found = True
                            break
                        if found == True: break

                if found == False:
                    # check if left over dublicate points are there
                    #print("Found=False: points =",points)
                    break

            # evtl noch invertieren
            faces.append(face)
            #print("FaceEnd")
    return faces

def computeTris(faces):
    def toPoint(p):
        #print("w=",p[3])
        return np.array([p[0],p[1],p[2]])

    tris = []
    for face in faces:
        #print("FACE:", face)
        for i in range(1,len(face) - 1):
            tris.append([toPoint(face[0]), toPoint(face[i]), toPoint(face[i+1])])
    return tris

def computePolygons(faces):
    polygons = []
    for face in faces:
        if len(face) == 0: continue
        p = []
        for x in face:
            p.append(np.array([x[0], x[1], x[2]]))
        polygons.append(p)
    #print("===========POLYS========",polygons)
    return polygons


def transformCube(corners):
    #print("corners:", corners)
    # parallelogram representation:
    # p0 + r * p1 + s * p2 * t * p3 + u * p4 with r,s,t,u in [0,1]
    p0 = transform(corners[0])
    p1 = rotate(np.array([corners[1][0] - corners[0][0], 0, 0, 0]))
    p2 = rotate(np.array([0, corners[1][1] - corners[0][1], 0, 0]))
    p3 = rotate(np.array([0, 0, corners[1][2] - corners[0][2], 0]))
    p4 = rotate(np.array([0, 0, 0, corners[1][3] - corners[0][3]]))

    #print("p0 p1 p2 p3:",p0,p1,p2,p3,p4)
    points = computePoints(p0, p1, p2, p3, p4)
    intersections = computeEdgeIntersections(p0, p1, p2, p3, p4, points)
    faces = computeFaces(points, intersections)
    return computePolygons(faces)

def renderPolygons(list, color):
    (cr,cg,cb) = color
    #print("LIST:", list)

    for poly in list:
        glBegin(GL_TRIANGLE_FAN)
        glColor3f(cr,cg,cb)
        for p in poly:
            glVertex3f(p[0], p[1], -p[2])
        glEnd()


def renderPolyLines(list):
    for poly in list:
        glBegin(GL_LINE_LOOP)
        glColor3f(1.,1.,1.)
        for p in poly:
            glVertex3f(p[0], p[1], -p[2])
        glEnd()

def renderLine( a,b,col ):
	glBegin(GL_LINES)
	glColor3f(col[0], col[1], col[2])
	glVertex3f(a[0], a[1], -a[2])
	glVertex3f(b[0], b[1], -b[2])
	glEnd()

def renderAxes():

    l = .2
    p = np.array([0,0,1,0])
    x = p + rotate(np.array([l,0,0,0]))
    y = p + rotate(np.array([0,l,0,0]))
    z = p + rotate(np.array([0,0,l,0]))
    w = p + rotate(np.array([0,0,0,l]))
    renderLine(p,x,(1,0,0))
    renderLine(p,y,(0,1,0))
    renderLine(p,z,(0,0,1))
    renderLine(p,w,(1,1,0))

pg.init()
scr = pg.display.set_mode((width,height), pg.OPENGL | pg.DOUBLEBUF)
pg.display.set_caption("HyperSpace Prototype 4: Sliced Perspective")


glEnable(GL_DEPTH_TEST)
glViewport(0,0,width,height)
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(70, 16.0/9.0, 0.1, 1000.0)
#glOrtho(-2,2,-2,2, -1, 1)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
glClearColor(0.0, 0.0, 0.0, 1.0)

clk = pg.time.Clock()
running = True
while running:
    clk.tick(60)
    for event in pg.event.get():
            if event.type == pg.QUIT: running = False
            if event.type == pg.KEYDOWN:
                    if event.key == pg.K_ESCAPE:
                            pg.event.post(pg.event.Event(pg.QUIT))

    bt = pg.key.get_pressed()
    ms = 0.05 # move speed
    ss = 0.01 # strafe speed (w direction)
    rs = 0.02 # rotate speed
    if bt[pg.K_RIGHT]: cam.rotXZ -= rs
    if bt[pg.K_LEFT]: cam.rotXZ += rs
    if bt[pg.K_UP]: cam.rotYZ -= rs
    if bt[pg.K_DOWN]: cam.rotYZ += rs
    if bt[pg.K_PAGEUP]:   cam.rotXW += rs; cam.rotZW += rs
    if bt[pg.K_PAGEDOWN]: cam.rotXW -= rs; cam.rotZW -= rs
    if bt[pg.K_w]: cam.pos += rotate(np.array([0,0,ms,0]),True)
    if bt[pg.K_s]: cam.pos += rotate(np.array([0,0,-ms,0]),True)
    if bt[pg.K_a]: cam.pos += rotate(np.array([-ms,0,0,0]),True)
    if bt[pg.K_d]: cam.pos += rotate(np.array([ms,0,0,0]),True)
    if bt[pg.K_q]: cam.pos += rotate(np.array([0,0,0,-ss]),True)
    if bt[pg.K_e]: cam.pos += rotate(np.array([0,0,0,ss]),True)
    if bt[pg.K_SPACE]: cam.pos += np.array([0,ms,0,0])
    if bt[pg.K_LSHIFT]: cam.pos -= np.array([0,ms,0,0])
    
    
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    
    geometry = []
    for (cube, color) in objects:
        geometry.append( (transformCube(cube), color) )

    for (polys, color) in geometry:
            renderPolygons(polys, color)

    glDisable(GL_DEPTH_TEST)

    for (polys, color) in geometry:
            renderPolyLines(polys)
    renderAxes()
    
    glBegin(GL_LINES)
    glColor3f(1,1,1)
    glVertex3f(0,0,1)
    glVertex3f(1,1,1)
    glEnd()

    glEnable(GL_DEPTH_TEST)

    pg.display.flip()

    print("XX", cam.pos)
